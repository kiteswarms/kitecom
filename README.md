# ZCM message definitions

*.zcm files are used to define ZCM messages. the ZCM Generator generates importable code files for a target language based on theses definition.

## How to add description to message definitions

You can add C++ comments to the description. The ZCM Generator ignores them. In case of generated C++ code all comments above to the definition are placed above the message class definition as one comment.

See logmessage_leveled.zcm  for example.

## Requirements

Please make sure to first install zcm and python3-setuptools-scm.
These are build dependencies that cmake requires to configure the project.

# Usage

## C
```cmake
target_link_libraries(your_target kitecom)
```

Attention! Include .h not .hpp
```c
#include <kitecom/kitecom_timestamped_vector_double.h>
```

## C++
```cmake
target_link_libraries(your_target kitecom::kitecom)
```

Attention! Include .hpp not .h
```cpp
#include <kitecom/timestamped_vector_double.hpp>
...
kitecom::timestamped_vector_double my_tsvector_instance;
...
```

## Python

```python
from kitecom import timestamped_vector_double
...
```
