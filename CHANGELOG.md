# Changelog

Until there is a dedicated guide how to use this log, please stick to this article:
https://keepachangelog.com/de/0.3.0/

Please pick from the following sections when categorizing your entry:
`Added`, `Changed`, `Removed`, `Fixed`

Before every entry put
`Major`, `Minor` or `Patch`
to mark the severity of the change.
## Unreleased

## 1.0.1 - 22.07-2021
### Added
- [Patch] Added license headers.

## 1.0.0 - 16.07-2021
### Added
- [Major] Added relevant messages needed for kite communication.
