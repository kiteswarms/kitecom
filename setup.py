from setuptools import setup, find_packages
from distutils.command.build_py import build_py as build_py

import subprocess

from pathlib import Path


class BuildZCM(build_py):

    def run(self):
        for package in self.packages:
            package_dir = Path(self.get_package_dir(package))
            zcm_files = list(map(str, package_dir.glob("*.zcm")))
            command = ["zcm-gen", "-p",
                       "--ppath", str(package_dir.parent),
                       "--package-prefix", package] + zcm_files
            subprocess.call(command)


class BuildZCMAndPython(build_py):

    def run(self):
        self.run_command("build_zcm")
        build_py.run(self)


setup(
    name="kiteswarms-kitecom",
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    description="python bindings for the kitecom messages",
    author="Maximilian Ernestus",
    author_email="maximilian@kiteswarms.com",
    package_dir={"": "src"},
    packages=["kitecom"],
    cmdclass={"build_zcm": BuildZCM, "build_py": BuildZCMAndPython, },
    # Some meta date for Debian compliance
    url="https://code.kiteswarms.com/kiteswarms/kitecom/",
    license="",  # NO LICENSE!!
    long_description="python bindings for the kitecom messages",
    python_requires=">=3.6",
)
