// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of kitecom.
//
//  kitecom is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  kitecom is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with kitecom. If not, see <https://www.gnu.org/licenses/>.

/*
copied from https://gitlab.com/CLIUtils/modern-cmake/-/blob/master/examples/extended-project/tests/#define CATCH_CONFIG_MAIN
*/
#include <assert.h>

#include <kitecom/commandmessage.hpp>

void test_basic_encoding_and_decoding() {
  auto msg = kitecom::commandmessage();
  msg.ts = 123;
  msg.command = "command1";
  msg.origin = "origin1";
  msg.destination = "destination1";

  char encoded[msg.getEncodedSize()];

  msg.encode(encoded, 0, msg.getEncodedSize());

  auto decoded = kitecom::commandmessage();
  decoded.decode(encoded, 0, msg.getEncodedSize());

  assert(msg.ts == decoded.ts);
  assert(msg.command == decoded.command);
  assert(msg.origin == decoded.origin);
  assert(msg.destination == decoded.destination);
}

int main() {
  test_basic_encoding_and_decoding();
}
